from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import Message
from django.shortcuts import render
...


def index(request):
    response = {}
    html = 'index2.html'
    response['message_form'] = Message_Form
    return render(request, html, response)


def add_message(request):
    response = {}
    form = Message_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):

        if request.POST['name'] != "":
            response['name'] = request.POST['name']
        else:
            response['name'] = "Anonymous"

        if request.POST['email'] != "":
            response['email'] = request.POST['email']
        else:
            response['email'] = "Anonymous"

        response['message'] = request.POST['message']
        message = Message(
            name=response['name'],
            email=response['email'],
            message=response['message'])
        message.save()
        html = 'form_result.html'
        return render(request, html, response)
    else:
        return HttpResponseRedirect('/guestBook')


def message_table(request):
    response = {}
    message = Message.objects.all()
    # for i in message:
    #     i.created_date_time
    response['message'] = message
    html = 'tables.html'
    return render(request, html, response)
