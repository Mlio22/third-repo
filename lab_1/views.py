from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'muhammat lio pratama'  # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
curr_month = int(datetime.now().strftime("%m"))
curr_day = int(datetime.now().strftime("%d"))
# TODO Implement this, format (Year, Month, Date)
birth_date = date(2001, 8, 22)
npm = 3141582622  # TODO Implement this
# Create your views here.


def index(request):
    response = {
        'page_name': 'Pengenalan diri Ajardia Beling',
        'name': mhs_name,
        'age': hitung_umur(birth_date.year, birth_date.month, birth_date.day),
        'npm': npm,
        'tempat_lahir': 'Jambi',
        'tanggal_lahir': birth_date.day,
        'bulan_lahir': tentukan_bulan(birth_date.month),
        'tahun_lahir': birth_date.year,
        'umur': hitung_umur(birth_date.year, birth_date.month, birth_date.day),
    }
    return render(request, 'profil2.html', response)


def hitung_umur(birth_year, birth_month, birth_day):
    # return curr_year - birth_year if birth_year <= curr_year else 0
    umur = curr_year - birth_year
    if curr_month - birth_month >= 0:
        if curr_day - birth_day >= 0:
            umur = umur
        else:
            return umur - 1
    else:
        return umur - 1
    return umur


def tentukan_bulan(birth_month):
    return {
        1: 'januari',
        2: 'februari',
        3: 'maret',
        4: 'april',
        5: 'mei',
        6: 'juni',
        7: 'juli',
        8: 'agustus',
        9: 'september',
        10: 'oktober',
        11: 'november',
        12: 'desember',
    }.get(birth_month, 'bulan not found')
