from django.shortcuts import render
from .models import Pesan
from django.http import HttpResponse, HttpResponseRedirect


def index(request):
    response = {
        "author": "Muhammat Lio Pratama"
        ""
    }
    return render(request, 'lab_4.html', response)


def tambahChat(request):
    if request.method == 'POST':

        if 'namaPesan' in request.POST:
            nama = request.POST['namaPesan']
        else:
            nama = 'gaada'
        pesan = request.POST['isiPesan']

        Pesan.objects.create(
            name=nama,
            message=pesan
        )

        response = {
            'namaisi': nama,
            'pesanisi': pesan,
            'token': token
        }
        return render(request, 'hasil.html', response)
    else:
        return HttpResponseRedirect('lab_4.html')

