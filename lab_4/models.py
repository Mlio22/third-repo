from django.db import models
from datetime import datetime

# Create your models here.


class Pesan(models.Model):
    name = models.CharField(max_length=50)
    message = models.TextField(max_length=200)
    createdTime = models.DateTimeField(
         default=datetime.now(tz=None))

    def __str__(self):
        return self.message
