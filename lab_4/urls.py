from django.urls import re_path
from .views import index, tambahChat

# kadang kalo diacak acak juga boleh sangad

app_name = 'lab_4'


# url for app
urlpatterns = [
    re_path(r'^$', index, name='index'),
    re_path(r'^/chat/tambah/$', tambahChat)
]
